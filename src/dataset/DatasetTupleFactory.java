package dataset;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.DecimalFormat;

import util.tuples.Cell;
import util.tuples.Tuple;
import util.tuples.TupleFactory;

public class DatasetTupleFactory implements TupleFactory {
    private final String inputFileName;
    private LineNumberReader reader;

    public DatasetTupleFactory(String inputFileName) {
        this.inputFileName = inputFileName;       
    }

    public void open() throws FileNotFoundException {
        reader = new LineNumberReader(new FileReader(inputFileName));
    }

    public void close() throws IOException {
        reader.close();
    }

    @Override
    public Tuple produce() {
        int pos;
        int objectId;
        double[] coordinates = new double[2];
        String text;
                
        String line;
        try {
            line = reader.readLine();
            if(line!=null){
                //parseLine
                try{
                	// TODO Denis Comentado - Usado no arq London
                   /* objectId = Integer.parseInt(line.substring(0, (pos = line.indexOf(' '))));  // 1
                    pos = line.indexOf(' ',pos+1); //skip the original object id. // 5
                    coordinates[0] = Double.parseDouble(line.substring(pos+1,(pos = line.indexOf(' ',pos+1)))); // 16
                    coordinates[1] = Double.parseDouble(line.substring(pos+1,(pos = line.indexOf(' ',pos+1)))); // 27
                    text = line.substring(pos+1);*/
                	
                	String[] split = line.split(" ");
                	
                	/*// Usado no USCities Census
                	objectId = Integer.parseInt(line.substring(0, (pos = line.indexOf(' ')))); // 1
                    //pos = line.indexOf(' ',pos+1); //skip the original object id.
                    text = line.substring(pos+1, (pos = line.indexOf(' ',pos+1))) + line.substring(pos, pos+3);
                    // invers�o somente da ordem de capta��o
                    coordinates[1] = Double.parseDouble(line.substring(pos = line.lastIndexOf(' ')));
                    coordinates[0] = Double.parseDouble(line.substring(line.lastIndexOf(' ', pos - 1), pos));*/
                    
                    // Usado no USCities Census
                	objectId = Integer.parseInt(split[0].trim()); // 1
                    //pos = line.indexOf(' ',pos+1); //skip the original object id.
                	DecimalFormat df = new DecimalFormat("#.00"); 
                    //text = String.format("%.4s", split[19].trim()) + " " + df.format(Double.parseDouble(split[87].trim())).replace(",", ".");
                	text = String.format("%.4s", split[19].trim());
                    //text = String.format(Locale.US, "%.1f", Double.parseDouble(split[19].trim()));
                    // invers�o somente da ordem de capta��o
                    coordinates[1] = Double.parseDouble(split[99].trim()); //87
                    coordinates[0] = Double.parseDouble(split[100].trim());
                    
                    //text = line.substring(pos+1);
                } catch (NumberFormatException e){
                    System.out.println("Invalid line='"+line+"'!!!");
                    return produce();
                }
                return new SpatialKeywordTuple(coordinates, text.toString(), objectId);
            }else{ //end of file reached
                return null;
            }
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public Cell[] getClustersMBRs() {
        return null;
    }
}
