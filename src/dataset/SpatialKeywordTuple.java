package dataset;

import util.tuples.Tuple;

public class SpatialKeywordTuple extends Tuple {

    private int id;
    private String text;

    public SpatialKeywordTuple(double[] values, String text, int id) {
        super(values);
        this.text = text;
        this.id = id;
    }
    
    public SpatialKeywordTuple(double[] values, String text, int id, Double value) {
        super(values);
        this.text = text;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

	public String toString() {
        return String.format("Tuple:%-5d  [%4.2f %5.2f] %5s ",id,this.getValue(0),this.getValue(1),text);
        //return String.format("%d %.8e %.8e %s ", id, this.getValue(0), this.getValue(1), text);
    }
}
