package dataset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.Locale;

public class USCities 
{
	public static void main(String[] args) throws Exception{
		LineNumberReader reader;
		
		//reader = new LineNumberReader(new FileReader("C:\\Users\\denis.ladeira\\Desktop\\algoritmos\\c++\\USCities-copia.txt;"));       
		File file = new File("C:\\Users\\denis.ladeira\\Desktop\\algoritmos\\c++\\USCities-copia.txt");
		
		// 90.3
		Double maior = 90.3;
		
		
		
		//String line;
		try(BufferedReader br = new BufferedReader(new FileReader(file))) 
		{
		    for(String line; (line = br.readLine()) != null; ) {
		    	//System.out.println(line.substring(0, 100));
		    	String[] split = line.split(" ");
		    	Double d = Double.parseDouble(split[19].trim());
		    	if(d < maior) {
		    		maior = d;
		    	}
		    }
		    // line is not visible here.
		}
		System.out.println(maior.toString());
		
		Double ini = 0.0;
		Double max = 10.0;
		
		while(ini <= max) {
			System.out.printf(Locale.US, "%.1f\n", ini);
			ini += 0.1;
		}
	}
}
