/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package s2i;

import framework.DefaultSearch;
import framework.SpatioTextualObject;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Properties;
import util.config.Settings;
import util.experiment.ExperimentException;
import util.sse.Vocabulary;
import util.statistics.DefaultStatisticCenter;
import util.statistics.StatisticCenter;

/**
 *
 * @author joao
 */
public class S2ISearch extends DefaultSearch{
	private final SpatialInvertedIndex s2i;

	public S2ISearch(StatisticCenter statisticCenter, boolean debug, Vocabulary termVocabulary,
			int numKeywords, int numResults, int numQueries, double alpha,
			double spaceMaxValue, boolean randomQueries, int numWarmUpQueries, SpatialInvertedIndex s2i){
		super(statisticCenter, debug, termVocabulary, numKeywords, numResults,
				numQueries, alpha, spaceMaxValue, randomQueries, numWarmUpQueries);

		this.s2i = s2i;
	}

	@Override
	public void open() throws ExperimentException {
		super.open();
	}

	@Override
	protected Iterator<SpatioTextualObject> execute(double queryLatitude,
			double queryLongitude, double maxDist, String queryKeywords, int k,
			double alpha) throws ExperimentException {
		try {
			Iterator<SpatioTextualObject>  queryResult=null;
			long time = System.currentTimeMillis();

			queryResult = s2i.search(queryLatitude, queryLongitude, maxDist, queryKeywords, k, alpha);

			statisticCenter.getTally("avgQueryProcessingTime").update(
					System.currentTimeMillis()-time);
			return queryResult;
		} catch (Exception ex) {
			throw new ExperimentException(ex);
		}
	}

	@Override
	protected int getDocumentFrequency(int termId) {
		try{
			return s2i.getDocumentFrequency(termId);
		}catch(Exception e){
			//throw new RuntimeException(e);
			return 0;
		}
	}

	@Override
	protected int getTotalNumObjects() {
		try{
			return s2i.getDatasetSize();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void collectStatistics(int count) {
		long treePageFaults =  statisticCenter.getCount("trees_pageFaults").getValue();

		statisticCenter.getTally("avgTreePageFault").update(treePageFaults/(double)count);

		long filePageFaults = statisticCenter.getCount("fileTermManager_map_blocksRead").getValue();

		statisticCenter.getTally("avgFilePageFault").update(filePageFaults/(double)count);

		statisticCenter.getTally("avgPageFault").update((treePageFaults+filePageFaults)/(double)count);

		long treeNodesAccessed = statisticCenter.getCount("trees_nodesAccessed").getValue();

		statisticCenter.getTally("avgTreeNodesAccessed").update(treeNodesAccessed/(double)count);

		long fileBlocksAccessed = statisticCenter.getCount("fileTermManagerBlocksAccessed").getValue();

		statisticCenter.getTally("avgFileBloksAcessed").update(fileBlocksAccessed/(double)count);
	}

	@Override
	public void close() throws ExperimentException {        
		try{
			this.s2i.close();
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	}

	public static void main(String[] args) throws Exception{
		/* STATISTICS
		 * BlocksAccessed
		 * _blocksRead
		 * ...termInfo
		 */
		//PrintStream out = new PrintStream(new FileOutputStream("k5_500Composta_results.txt"));
		//System.setOut(out);

		Properties properties = Settings.loadProperties("s2i.properties");
		Integer method = Integer.parseInt(properties.getProperty("query.methodExperiment"));
		if(method == 1) {
			for(int k = 5; k <= 5; k+=5)
			{
				DefaultStatisticCenter statistics = new DefaultStatisticCenter();

				SpatialInvertedIndex s2i = new SpatialInvertedIndex(statistics,
						properties.getProperty("s2i.folder"),
						Integer.parseInt(properties.getProperty("s2i.blockSize")),
						Integer.parseInt(properties.getProperty("s2i.tree.dimensions")),
						Integer.parseInt(properties.getProperty("s2i.tree.cacheSize")),
						Integer.parseInt(properties.getProperty("s2i.tree.minNodeCapacity")),
						Integer.parseInt(properties.getProperty("s2i.tree.maxNodeCapacity")),
						Integer.parseInt(properties.getProperty("s2i.fileCacheSize")),
						Integer.parseInt(properties.getProperty("s2i.treesOpen")),
						false/*searchingTime*/);

				s2i.open();

				S2ISearch searching = new S2ISearch(statistics,
						Boolean.parseBoolean(properties.getProperty("experiment.debug")),
						s2i.getTermVocabulary(),
						Integer.parseInt(properties.getProperty("query.numKeywords")),
						Integer.parseInt(properties.getProperty("query.numResults")),
						Integer.parseInt(properties.getProperty("query.numQueries")),
						Double.parseDouble(properties.getProperty("query.alfa")),
						Double.parseDouble(properties.getProperty("dataset.spaceMaxValue")),
						Boolean.parseBoolean(properties.getProperty("queries.random")),
						Integer.parseInt(properties.getProperty("query.numWarmUpQueries")),
						s2i);

				searching.open();
				searching.runknn(k, false);

				System.out.println("\n\nStatistics:\n"+statistics.getStatus(k, "74.5%/90.08%"));

				searching.close();

				// TODO
			}
		}
		else
		{
			Boolean composto = true;
			double mat[][];
            
            if(composto)
            {
            	mat = new double[][] { 
                    { 75.16, 2.6 },
                    { 76.67, 2.7 },
                    { 80.75, 2.8 },
                    { 84.45, 2.85 },
                    { 88.59, 2.9 },
                    { 94.42, 3.0 },
                    { 97.55, 3.1 },
                    { 98.98, 3.2 },
                    { 99.61, 3.3 }
                };	
            }
            else 
            {
                mat = new double [][] { {50, 31.0, 39.1}, {51, 33.0, 40.1}, {52, 37.0, 46.1}, 
            		{53, 27.0, 38.1}, {54, 35.0, 41.1}, {55, 29.0, 38.1}, {56, 38.0, 48.1}, {57, 38.0, 47.1}, 
            		{58, 24.0, 37.1}, {59, 31.0, 38.1}, {60, 39.0, 58.1}, {61, 39.0, 54.1}, {62, 35.0, 40.1}, 
            		{63, 39.0, 50.1}, {64, 30.0, 37.1}, {65, 37.0, 42.1}, {66, 31.0, 37.1}, {67, 38.0, 43.1}, 
            		{68, 36.0, 40.1}, {69, 32.0, 37.1}, {70, 40.0, 49.1}, {71, 40.0, 48.1}, {72, 21.0, 35.1}, 
            		{73, 30.0, 36.1}, {74, 24.0, 35.1}, {75, 41.0, 55.1}, {76, 40.0, 45.1}, {77, 41.0, 50.1}, 
            		{78, 41.0, 49.1}, {79, 41.0, 48.1}, {80, 42.0, 54.1}, {81, 42.0, 51.1}, {82, 26.0, 34.1}, 
            		{83, 21.0, 33.1}, {84, 23.0, 33.1}, {85, 29.0, 34.1}, {86, 26.0, 33.1}, {87, 18.0, 32.1}, 
            		{88, 22.0, 32.1}, {89, 29.0, 33.1}, {90, 20.0, 31.1}, {91, 38.0, 39.1}, {92, 40.0, 41.1}, 
            		{93, 37.0, 38.1}, {94, 35.0, 36.1}, {95, 42.0, 43.1}, {96, 33.0, 34.1}, {97, 44.0, 45.1}, 
            		{98, 46.0, 47.1}, {99, 47.0, 48.1}, {99.5, 23.0, 24.1}, {99.75, 56.0, 57.1}, {99.9, 19.0, 20.1} };
            		
            		//mat = new double [][] { {82, 26.0, 34.1}, {89, 29.0, 33.1} };
            }
			
            for(int k = 0; k < mat.length; k+=1)
            {
            	DefaultStatisticCenter statistics = new DefaultStatisticCenter();

            	SpatialInvertedIndex s2i = new SpatialInvertedIndex(statistics,
            			properties.getProperty("s2i.folder"),
            			Integer.parseInt(properties.getProperty("s2i.blockSize")),
            			Integer.parseInt(properties.getProperty("s2i.tree.dimensions")),
            			Integer.parseInt(properties.getProperty("s2i.tree.cacheSize")),
            			Integer.parseInt(properties.getProperty("s2i.tree.minNodeCapacity")),
            			Integer.parseInt(properties.getProperty("s2i.tree.maxNodeCapacity")),
            			Integer.parseInt(properties.getProperty("s2i.fileCacheSize")),
            			Integer.parseInt(properties.getProperty("s2i.treesOpen")),
            			false/*searchingTime*/);

            	s2i.open();

            	S2ISearch searching = new S2ISearch(statistics,
            			Boolean.parseBoolean(properties.getProperty("experiment.debug")),
            			s2i.getTermVocabulary(),
            			Integer.parseInt(properties.getProperty("query.numKeywords")),
            			Integer.parseInt(properties.getProperty("query.numResults")),
            			Integer.parseInt(properties.getProperty("query.numQueries")),
            			Double.parseDouble(properties.getProperty("query.alfa")),
            			Double.parseDouble(properties.getProperty("dataset.spaceMaxValue")),
            			Boolean.parseBoolean(properties.getProperty("queries.random")),
            			Integer.parseInt(properties.getProperty("query.numWarmUpQueries")),
            			s2i);

            	searching.open();
            	searching.runSelectivity(k, composto, mat);

            	System.out.println("\n\nStatistics:\n"+statistics.getStatus(100, String.valueOf(mat[k][0])));

            	searching.close();
            }
		}


		/* Properties properties = Settings.loadProperties("s2i.properties");
        DefaultStatisticCenter statistics = new DefaultStatisticCenter();

        SpatialInvertedIndex s2i = new SpatialInvertedIndex(statistics,
                    properties.getProperty("s2i.folder"),
                    Integer.parseInt(properties.getProperty("s2i.blockSize")),
                    Integer.parseInt(properties.getProperty("s2i.tree.dimensions")),
                    Integer.parseInt(properties.getProperty("s2i.tree.cacheSize")),
                    Integer.parseInt(properties.getProperty("s2i.tree.minNodeCapacity")),
                    Integer.parseInt(properties.getProperty("s2i.tree.maxNodeCapacity")),
                    Integer.parseInt(properties.getProperty("s2i.fileCacheSize")),
                    Integer.parseInt(properties.getProperty("s2i.treesOpen")),
                    falsesearchingTime);

        s2i.open();

        S2ISearch searching = new S2ISearch(statistics,
                    Boolean.parseBoolean(properties.getProperty("experiment.debug")),
                    s2i.getTermVocabulary(),
                    Integer.parseInt(properties.getProperty("query.numKeywords")),
                    Integer.parseInt(properties.getProperty("query.numResults")),
                    Integer.parseInt(properties.getProperty("query.numQueries")),
                    Double.parseDouble(properties.getProperty("query.alfa")),
                    Double.parseDouble(properties.getProperty("dataset.spaceMaxValue")),
                    Boolean.parseBoolean(properties.getProperty("queries.random")),
                    Integer.parseInt(properties.getProperty("query.numWarmUpQueries")),
                    s2i);

        searching.open();*/

		//searching.runknn(10, false);
		//System.out.println("\n\nStatistics:\n"+statistics.getStatus());

		//searching.close();

		/*List<String> lines = Arrays.asList("12112" + "\t" + "212412" + "\t" + "212412" + "\t" + "999999",
        		"12112" + "\t" + "212412" + "\t" + "212412" + "\t" + "999999");
        Path file = Paths.get("the-file-name.txt");
        Files.write(file, lines, Charset.forName("UTF-8"));*/
	}

	@Override
	public void run() throws ExperimentException {
		// TODO Auto-generated method stub

	}
}
