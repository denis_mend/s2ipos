/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package framework;

import java.io.FileReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map.Entry;

import util.RandomUtil;
import util.Util;
import util.experiment.Experiment;
import util.experiment.ExperimentException;
import util.experiment.ExperimentResult;
import util.sse.SSEExeption;
import util.sse.Vocabulary;
import util.statistics.StatisticCenter;
import xxl.core.spatial.points.DoublePoint;
import xxl.core.spatial.points.Point;

import util.config.Settings;
import java.util.Properties;

/**
 *
 * @author joao
 */
public abstract class DefaultSearch implements Experiment {
    private HashMap<Integer, TermEntry> termHash;
    protected final Vocabulary termVocabulary;
    protected final StatisticCenter statisticCenter;
    protected ArrayList<ExperimentResult> result;

    private final int numQueries;
    
    private final double spaceMaxValue;

    private final double spaceMaxDistance;

    private int datasetSize=-1;
    protected final int numKeywords;
    protected final int numResults;
    protected final double alpha;

    private final boolean randomQueries;
    private final boolean debugMode;
    private boolean warmUp;
    private final int numWarmUpQueries;
    private int iterations;

    public DefaultSearch(StatisticCenter statisticCenter, boolean debugMode, Vocabulary termVocabulary,
            int numKeywords, int numResults, int numQueries, double alpha, 
            double spaceMaxValue, boolean randomQueries, int numWarmUpQueries){
        this.termVocabulary = termVocabulary;
        this.statisticCenter = statisticCenter;
        this.numKeywords = numKeywords;
        this.numResults = numResults;
        this.numQueries = numQueries;
        this.alpha = alpha;
        this.spaceMaxValue = spaceMaxValue;
        this.debugMode = debugMode;
        this.randomQueries = randomQueries;
        this.numWarmUpQueries = numWarmUpQueries;


        //The max distance is the diagonal of the square whose sides are spaceMaxValue
        this.spaceMaxDistance = Math.sqrt(spaceMaxValue*spaceMaxValue+ spaceMaxValue*spaceMaxValue);
    }


    public void open() throws ExperimentException {
        termHash = new HashMap<Integer, TermEntry>();
        int termId;
        int df;
        for(Entry<String,Integer> entry:termVocabulary.entrySet()){
            termId=entry.getValue();
            df = getDocumentFrequency(termId);
            if(df>0){
                termHash.put(termId, new TermEntry(entry.getKey(), df));
            }
        }

        if(numWarmUpQueries>0){
            warmUp = true;
            iterations = numWarmUpQueries; 
        }else{
            warmUp=false;
            iterations = numQueries;
        }
    }

    public final void runknn(int k, boolean composto) throws ExperimentException {
        try{
            long time;
            String queryKeywords;
            Point point;
            result = null;
            Iterator<SpatioTextualObject> spatialObjects=null;

            if(debugMode && warmUp) System.out.println("\n[WarmUp, queries="+iterations+"]");
            
            //TODO Denis
            //LineNumberReader reader = new LineNumberReader(new FileReader("C:\\Users\\denis.ladeira\\Desktop\\algoritmos\\c++\\USCities-2kCensus-500centers.txt"));
            
            Properties properties = Settings.loadProperties("s2i.properties");
            LineNumberReader reader = new LineNumberReader(new FileReader(properties.getProperty("dataset.queryPath")));
            
            for(int i=0;i<iterations;i++)
            {
                // TODO Denis
            	//point= nextQueryPoint();
            	String[] text = reader.readLine().split(" ");
                
                Double ini = 36.0;
        		Double max = 37.1;
        		String range = "";
        		while(ini <= max) {
        			range += String.format(Locale.US, "%.1f", ini) + ",";
        			ini += 0.1;
        		}
        		
        		if(composto) {
        			/*ini = 2.91;
        			//max = 4.84;
        			max = 8.10;
            		while(ini <= max) {
            			range += String.format(Locale.US, "%.2f", ini) + ",";
            			ini += 0.01;
            		}*/

            		/*range += "4.86, 4.87, 4.88, 4.89, 4.90, 4.92, 4.93, 4.94, 4.95, 4.97, 5.00, 5.03," +
            				"5.04, 5.08, 5.10, 5.13, 5.16, 5.17, 5.19, 5.22, 5.24, 5.25, 5.34, 5.35, 5.37," + 
            				"5.38, 5.43, 5.45, 5.47, 5.48, 5.50, 5.51, 5.52, 5.60, 5.62, 5.66, 5.73, 5.81, 5.84," +
            				"5.96, 5.98, 5.99, 6.02, 6.04, 6.53, 6.60, 7.58, 8.10,"; */
        			
        			ini = 2.91;
        			max = 8.10;
            		while(ini <= max) {
            			range += String.format(Locale.US, "%.2f", ini) + ",";
            			ini += 0.01;
            		}

            		/*range += "4.86, 4.87, 4.88, 4.89, 4.90, 4.92, 4.93, 4.94, 4.95, 4.97, 5.00, 5.03," +
            				"5.04, 5.08, 5.10, 5.13, 5.16, 5.17, 5.19, 5.22, 5.24, 5.25, 5.34, 5.35, 5.37," + 
            				"5.38, 5.43, 5.45, 5.47, 5.48, 5.50, 5.51, 5.52, 5.60, 5.62, 5.66, 5.73, 5.81, 5.84," +
            				"5.96, 5.98, 5.99, 6.02, 6.04, 6.53, 6.60, 7.58, 8.10,";*/
        			
        			//range = "4.85, 4.91,";
        		}
        		
                queryKeywords = "[" + range.substring(0, range.length()-1) + "]";
                
                point = new DoublePoint(new double[]{Double.parseDouble(text[100].trim()), Double.parseDouble(text[99].trim())});
                
                if(debugMode){
                    System.out.print("# query "+(i+1)+"/"+iterations+" = ['"+queryKeywords+"'("+
                        Util.cast(point.getValue(0),3)+","+Util.cast(point.getValue(1),3)+")... ");
                }
                                
                time = System.currentTimeMillis();

                /*spatialObjects = execute(point.getValue(0), point.getValue(1), 
                        spaceMaxDistance, queryKeywords, numResults, alpha);*/
                spatialObjects = execute(point.getValue(0), point.getValue(1), 
                        spaceMaxDistance, queryKeywords, k, alpha);
                
                if(debugMode) System.out.println(Util.time(System.currentTimeMillis()-time)+"]");
                
                if(debugMode){
                	result = new ArrayList();
                	for(int v=0; spatialObjects.hasNext(); v++){
                		result.add(spatialObjects.next());
                		System.out.println("-->["+(v+1)+"]  " +result.get(v).toString());
                	}
                }
            }

            if(warmUp){
                warmUp=false;
                iterations = numQueries;
                statisticCenter.resetStatistics();
                if(debugMode) System.out.println("\n[For real! queries="+numQueries+"]");
                run();
            }

            reader.close();
            collectStatistics(iterations);

            if(result==null){
                result = new ArrayList();
                for(int i=0; spatialObjects.hasNext(); i++){
                    result.add(spatialObjects.next());
                }
            }
        }catch(Exception e){
            throw new ExperimentException(e);
        }
    }
    
    public final void runSelectivity(int k, boolean composto, double mat[][]) throws ExperimentException 
    {
        try{
        	long time;
            String queryKeywords;
            Point point;
            result = null;
            Iterator<SpatioTextualObject> spatialObjects=null;

            if(debugMode && warmUp) System.out.println("\n[WarmUp, queries="+iterations+"]");
            
            //TODO Denis
            //LineNumberReader reader = new LineNumberReader(new FileReader("C:\\Users\\denis.ladeira\\Desktop\\algoritmos\\c++\\USCities-2kCensus-500centers.txt"));
            
            Properties properties = Settings.loadProperties("s2i.properties");
            LineNumberReader reader = new LineNumberReader(new FileReader(properties.getProperty("dataset.queryPath")));
            
            for(int i=0;i<iterations;i++)
            {
            	String[] text = reader.readLine().split(" ");

            	Double ini;
            	Double max;
            	String range = "";

            	if(composto) 
            	{
            		ini = 40.0;
            		max = 48.1;
            		while(ini <= max) {
            			range += String.format(Locale.US, "%.1f", ini) + ",";
            			ini += 0.1;
            		}

            		ini = mat[k][1];
            		//max = 4.84;
            		max = 8.10;
            		while(ini <= max) {
            			range += String.format(Locale.US, "%.2f", ini) + ",";
            			ini += 0.01;
            		}

            		/*range += "4.86,4.87,4.88,4.89,4.90,4.92,4.93,4.94,4.95,4.97,5.00,5.03," +
            				"5.04,5.08,5.10,5.13,5.16,5.17,5.19,5.22,5.24,5.25,5.34,5.35,5.37," + 
            				"5.38,5.43,5.45,5.47,5.48,5.50,5.51,5.52,5.60,5.62,5.66,5.73,5.81,5.84," +
            				"5.96,5.98,5.99,6.02,6.04,6.53,6.60,7.58,8.10,"; */
            	}
            	else {
            		ini = mat[k][1];
            		max = mat[k][2];
            		
            		max = max + 0.1;
        			while(ini <= max) {
        				range += String.format(Locale.US, "%.1f", ini) + ",";
        				ini += 0.1;
        			}

            		/*double[] vet = new double[] { 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 25.4, 26.0,
            				26.3, 26.7, 26.9, 27.0, 27.3, 27.4, 27.5, 27.6, 27.7, 27.9, 28.0, 28,2, 28.4,
            				28.5, 28.6, 28.7, 28.9, 29.0, 29.2, 29.3, 29.4, 29.5, 29.6, 29.7, 29.8, 29.9,
            				30.0, 30.2, 30.3, 30.4, 30.6, 30.7, 30.8, 30.9 };

            		if(ini < 31.0) {
            			if(max < 31.0) 
            			{ 
            				for(double d : vet) {
            					if(d >= ini && d <= max) {
            						range += String.format(Locale.US, "%.1f", d) + ",";
            					}
            				}
            			}
            			else {
            				for(double d : vet) {
            					if(d >= ini) {
            						range += String.format(Locale.US, "%.1f", d) + ",";
            					}
            				}

            				ini = 31.0;
            				max = max + 0.1;
            				while(ini <= max) {
            					range += String.format(Locale.US, "%.1f", ini) + ",";
            					ini += 0.1;
            				}
            			}
            		}
            		else {
            			max = max + 0.1;
            			while(ini <= max) {
            				range += String.format(Locale.US, "%.1f", ini) + ",";
            				ini += 0.1;
            			}
            		} */
            	}

            	queryKeywords = "[" + range.substring(0, range.length()-1) + "]";

            	point = new DoublePoint(new double[]{Double.parseDouble(text[100].trim()), Double.parseDouble(text[99].trim())});

            	if(debugMode){
            		System.out.print("# query "+(i+1)+"/"+iterations+" = ['"+queryKeywords+"'("+
            				Util.cast(point.getValue(0),3)+","+Util.cast(point.getValue(1),3)+")... ");
            	}

            	time = System.currentTimeMillis();

            	spatialObjects = execute(point.getValue(0), point.getValue(1), 
            			spaceMaxDistance, queryKeywords, 100, alpha);

            	if(debugMode) System.out.println(Util.time(System.currentTimeMillis()-time)+"]");

            	if(debugMode){
            		result = new ArrayList();
            		for(int v=0; spatialObjects.hasNext(); v++){
            			result.add(spatialObjects.next());
            			System.out.println("-->["+(v+1)+"]  " +result.get(v).toString());
            		}
            	}
            }

            if(warmUp){
            	warmUp=false;
            	iterations = numQueries;
            	statisticCenter.resetStatistics();
            	if(debugMode) System.out.println("\n[For real! queries="+numQueries+"]");
            	run();
            }

            reader.close();
            collectStatistics(iterations);

            if(result==null){
            	result = new ArrayList();
            	for(int i=0; spatialObjects.hasNext(); i++){
            		result.add(spatialObjects.next());
            	}
            }
        }catch(Exception e){
        	throw new ExperimentException(e);
        }
    }

    protected abstract void collectStatistics(int numQueries);

    /**
     * Returns the number of documents in which the term appears.
     * @param termId
     * @return
     */
    protected abstract int getDocumentFrequency(int termId);

    /**
     * Returns the total number of objects in the dataset
     * @return
     */
    protected abstract int getTotalNumObjects();


    /**
     * Implement the query processing
     * @param queryLatitude
     * @param queryLongitude
     * @param queryVector
     * @param queryLengt the total number of terms in the query
     * @param k
     * @param alpha
     * @return
     * @throws ExperimentException
     */
    protected abstract  Iterator<SpatioTextualObject> execute(double queryLatitude,
            double queryLongitude, double maxDist,  String queryKeywords,
            int k, double alpha) throws ExperimentException;


    public ExperimentResult[] getResult() {
        return result.toArray(new ExperimentResult[result.size()]);
    }

    /**
     * Produces a random point in the spaceMaxValue
     * @return
     */
    private Point nextQueryPoint() {
        return new DoublePoint(new double[]{RandomUtil.nextDouble(spaceMaxValue),
                                            RandomUtil.nextDouble(spaceMaxValue)});
    }


    private String mostFrequentQueryKeywords(int numKeywords) throws SSEExeption{
        String[] keywords = new String[numKeywords];
        TermEntry entry;
        double termProbability;        
        datasetSize = datasetSize==-1 ? getTotalNumObjects() : datasetSize;
        for(int i=0;i<numKeywords;i++){
            do{
                while((entry = termHash.get(RandomUtil.nextInt(termHash.size())))==null);
                termProbability=entry.getDocumentFrequency()/(double)datasetSize;
            }while(RandomUtil.nextDouble(1)>termProbability);
            keywords[i] = entry.getTerm();
        }        
        return Arrays.toString(keywords);
    }

    private String randomQueryKeywords(int numKeywords) {
        String[] keywords = new String[numKeywords];
        for(int i=0;i<numKeywords;i++){
            keywords[i] = termHash.get(RandomUtil.nextInt(termHash.size())).getTerm();
        }
        return Arrays.toString(keywords);
    }

    class TermEntry{
        private final String term ;
        private final int docFrequency;

        public TermEntry(String term, int docFrequency){
            this.term = term;
            this.docFrequency = docFrequency;
        }

        public String getTerm() {
            return term;
        }

        public int getDocumentFrequency() {
            return docFrequency;
        }

        @Override
        public String toString(){
            return term+"="+docFrequency;
        }
    }

}
